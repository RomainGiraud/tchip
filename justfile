BUILD := env_var_or_default("BUILD", "Release")
GENERATOR := env_var_or_default("GENERATOR", "Ninja")
SAMURAI_DEFINE := if env_var_or_default("WITH_SAMURAI", "false") != "false" { "-DCMAKE_MAKE_PROGRAM=samu" } else { "" }

default:
    @just --list

init:
    git submodule init
    git submodule update
    ./vcpkg/bootstrap-vcpkg.sh -disableMetrics
    ./emsdk/emsdk install latest
    ./emsdk/emsdk activate latest

update:
    git submodule update --recursive --remote

build-wasm:
    #!/usr/bin/env bash
    source ./emsdk/emsdk_env.sh
    cmake -S . -B build-wasm -G Ninja {{SAMURAI_DEFINE}} "-DVCPKG_CHAINLOAD_TOOLCHAIN_FILE=$PWD/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake" "-DCMAKE_TOOLCHAIN_FILE=$PWD/vcpkg/scripts/buildsystems/vcpkg.cmake" -DVCPKG_TARGET_TRIPLET=wasm32-emscripten -DVCPKG_MANIFEST_FEATURES=wasm -DCMAKE_BUILD_TYPE={{BUILD}}
    cmake --build build-wasm

run-wasm: build-wasm
    #!/usr/bin/env bash
    source ./emsdk/emsdk_env.sh
    emrun build-wasm/tchip.html

build:
    cmake -S . -B build -G Ninja {{SAMURAI_DEFINE}} "-DCMAKE_TOOLCHAIN_FILE=$PWD/vcpkg/scripts/buildsystems/vcpkg.cmake" -DVCPKG_MANIFEST_FEATURES=host -DCMAKE_BUILD_TYPE={{BUILD}}
    cmake --build build

run: build
    ./build/tchip ./games/INVADERS

test:
    ./build/tests

clean:
    @rm ./build/ ./build-wasm/ -rf

docker-build:
    docker buildx build . -t tchip8-dev:latest

docker-run:
    docker run --rm -it -v $(realpath ./):/workspace:z tchip8-dev bash
