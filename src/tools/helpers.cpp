#include "helpers.h"

#include <fstream>

using namespace std;

std::pair<std::unique_ptr<char[]>, size_t> tchip::tools::helpers::getFileContent(const std::string &filename)
{
    ifstream fs (filename.c_str());
    if (!fs)
        throw runtime_error("loadFile: file \"" + filename + "\" doesn't exist");

    filebuf *pbuf = fs.rdbuf();

    size_t size = pbuf->pubseekoff(0, ios::end, ios::in);
    pbuf->pubseekpos(0, ios::in);

    auto src = std::make_unique<char[]>(size+1);
    long ss = pbuf->sgetn(src.get(), size);
    fs.close();
    src[ss] = '\0';

    return make_pair(std::move(src), size);
}

size_t tchip::tools::helpers::getFileContentInto(const std::string &filename, char* data, unsigned data_size, int offset)
{
    ifstream fs (filename.c_str());
    if (!fs)
        throw runtime_error("loadFile: file \"" + filename + "\" doesn't exist");

    filebuf *pbuf = fs.rdbuf();

    size_t size = pbuf->pubseekoff(0, ios::end, ios::in);
    pbuf->pubseekpos(0, ios::in);

    if ((offset + size) > data_size)
        return 0;

    auto src = data + offset;
    long ss = pbuf->sgetn(src, size);
    fs.close();
    src[ss] = '\0';

    return size;
}
