#ifndef TCHIP_SCOPEGUARD_H
#define TCHIP_SCOPEGUARD_H

#include <memory>
#include <string>
#include <utility>

namespace tchip::tools
{

template <typename T>
class ScopeGuard
{
public:
    ScopeGuard(T&& fn)
        : _fn(std::forward<T>(fn))
    {}

    ~ScopeGuard()
    {
        _fn();
    }

    ScopeGuard(const ScopeGuard&) = delete;
    ScopeGuard(ScopeGuard&&) = delete;
    ScopeGuard& operator=(const ScopeGuard&) = delete;
    ScopeGuard& operator=(ScopeGuard&&) = delete;

private:
    T _fn;
};

}

#endif /*TCHIP_SCOPEGUARD_H*/
