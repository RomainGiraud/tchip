#ifndef TCHIP_HELPERS_H
#define TCHIP_HELPERS_H

#include <memory>
#include <string>
#include <utility>

namespace tchip::tools::helpers
{

std::pair<std::unique_ptr<char[]>, size_t> getFileContent(const std::string &filename);
size_t getFileContentInto(const std::string &filename, char* data, unsigned data_size, int offset);

}

#endif /*TCHIP_HELPERS_H*/
