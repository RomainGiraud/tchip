#include "shader.h"

#include <exception>
#include <iostream>
#include <string>

#include <tools/helpers.h>

using namespace std;

tchip::Shader::Shader(Type type)
    : _id(0)
{
    switch (type)
    {
        case Type::Vertex:
            _type = GL_VERTEX_SHADER;
            break;
        case Type::Geometry:
           _type = GL_GEOMETRY_SHADER;
           break;
        case Type::Fragment:
            _type = GL_FRAGMENT_SHADER;
            break;
    }
}

tchip::Shader::~Shader()
{
    glDeleteShader(_id);
}

void tchip::Shader::create()
{
    _id = glCreateShader(_type);
}

void tchip::Shader::loadFile(const string &filename)
{
    auto src = tools::helpers::getFileContent(filename);
    compile(src.first.get());
}

void tchip::Shader::loadSource(const string &source)
{
    compile(source.c_str());
}

void tchip::Shader::compile(const char *src)
{
    glShaderSource(_id, 1, &src, NULL);
    glCompileShader(_id);

    GLint result;
    glGetShaderiv(_id, GL_COMPILE_STATUS, &result);

    if (!result)
    {
        char infoLog[1024];
        glGetShaderInfoLog(_id, 1024, 0, infoLog);
        cerr << "Shader compilation at " << _id << ":" << endl
             << infoLog << endl;

        glDeleteShader(_id);

        throw runtime_error("compileShader: glCompileShader");
    }
}

GLuint tchip::Shader::id() const
{
    return _id;
}
