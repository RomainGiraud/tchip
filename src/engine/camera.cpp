#include "camera.h"

#include <iostream>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

using namespace std;

tchip::Camera::Camera()
{
  _zoom = 1;
  _size = glm::vec2(2, 2);
  updateViewMatrix();
  updateProjectionMatrix();
}

tchip::Camera::~Camera()
{
}

void tchip::Camera::update(double time)
{
  if (_position != _targetPosition)
  {
    _position = glm::mix(_position, _targetPosition, (float)time * 3);
    updateViewMatrix();
  }
}

void tchip::Camera::updateProjectionMatrix()
{
  float halfx = (_size.x / 2.0f) * _zoom;
  float halfy = (_size.y / 2.0f) * _zoom;
  // left, right, bottom, top, znear, zfar
  _projectionMatrix = glm::ortho(-halfx, halfx, -halfy, halfy, 0.f, -100.f);
}

void tchip::Camera::updateViewMatrix()
{
  _viewMatrix = glm::translate(glm::mat4(1.0), _position);
}

void tchip::Camera::setSize(glm::ivec2 size)
{
  _size = size;
  updateProjectionMatrix();
}

glm::vec3 tchip::Camera::getTargetPosition() const
{
  return _targetPosition;
}

void tchip::Camera::setTargetPosition(glm::vec3 p)
{
  _targetPosition = p;
}

glm::mat4 tchip::Camera::getProjectionMatrix() const
{
  return _projectionMatrix;
}

glm::mat4 tchip::Camera::getViewMatrix() const
{
  return _viewMatrix;
}

glm::vec3 tchip::Camera::getPosition() const
{
  return _position;
}

void tchip::Camera::setPosition(glm::vec3 p)
{
  //_position = _targetPosition = p;
  _position = p;
  updateViewMatrix();
}

float tchip::Camera::getZoom() const
{
  return _zoom;
}

void tchip::Camera::setZoom(float z)
{
  _zoom += z / 50;
  if (_zoom < 0.05)
    _zoom = 0.05f;

  updateProjectionMatrix();
}
