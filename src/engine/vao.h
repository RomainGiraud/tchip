#ifndef TCHIP_VAO_H
#define TCHIP_VAO_H

#include <GL/glad.h>

namespace tchip
{

class Vao
{
public:
    Vao();
    ~Vao();

    void create();
    void bind();
    void release();

private:
    GLuint _id;
};

}

#endif /*TCHIP_VAO_H*/
