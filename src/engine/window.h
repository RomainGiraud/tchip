#ifndef TCHIP_WINDOW_H
#define TCHIP_WINDOW_H

#include <list>
#include <string>

#include <glm/glm.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <GL/glad.h>

#include "camera.h"

namespace tchip
{

class Window
{
public:
    explicit Window(const std::string& title = "My window", int width = 800, int height = 600);
    virtual ~Window();

    void printOglInfo() const;
    int run();
    void close();

    void cursorShow();
    void cursorHide();
    glm::vec2 getMousePosition();
    void setMousePosition(glm::vec2 v);
    glm::ivec2 getSize();

private:
    void initOgl();

    // FPS tool
    static const int fpsSize = 100;
    int fpsList[fpsSize];
    int fpsIndex = 0;
    int fpsSum = 0;
    double computeFps(int value);

    // Callbacks
    static void framebufferSizeProxy(GLFWwindow *window, int w, int h);
    static void keyProxy(GLFWwindow *window, int key, int scancode, int action, int mods);
    static void mouseWheelProxy(GLFWwindow *window, double x, double y);
    static void oglErrorProxy(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message, const void* userParam);
    void resize(int w, int h);
    void oglError(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message) const;

#ifdef __EMSCRIPTEN__
    static void wasm_update(void *arg);
#endif

protected:
    int width;
    int height;
    GLFWwindow *window;

    virtual void mouseWheel(double x, double y) {}
    virtual void key(int key, int scancode, int action, int mods) {}

    virtual void initialize() {}
    virtual bool update() { return true; }
};

}

#endif /*TCHIP_WINDOW_H*/
