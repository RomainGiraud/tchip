#ifndef TCHIP_BUFFER_H
#define TCHIP_BUFFER_H

#include <GL/glad.h>

#include <cstddef>

namespace tchip
{

class Buffer
{
public:
    enum class Type
    {
        Vertex,
        Index
    };

    enum class Usage
    {
        StreamDraw,
        StaticDraw,
        DynamicDraw,
        StreamRead,
        StaticRead,
        DynamicRead,
        StreamCopy,
        StaticCopy,
        DynamicCopy,
    };

    explicit Buffer(Type type = Type::Vertex, Usage usage = Usage::StaticDraw);
    ~Buffer();

    void create();
    void bind();
    void release();
    void attach(const void *data, size_t size);

private:
    GLuint _id;
    GLenum _type;
    GLenum _usage;
};

}

#endif /*TCHIP_BUFFER_H*/
