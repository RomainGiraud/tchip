#ifndef TCHIP_SHADER_H
#define TCHIP_SHADER_H

#include <GL/glad.h>

#include <string>

namespace tchip
{

class Shader
{
public:
    enum class Type
    {
        Vertex,
        Geometry,
        Fragment
    };

    explicit Shader(Type type);
    ~Shader();

    void create();
    void loadFile(const std::string &filename);
    void loadSource(const std::string &source);

    GLuint id() const;

private:
    GLuint _id;
    GLenum _type;

    void compile(const char *src);
};

}

#endif /*TCHIP_SHADER_H*/
