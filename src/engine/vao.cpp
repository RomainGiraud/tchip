#include "vao.h"

tchip::Vao::Vao()
    : _id(0)
{
}

tchip::Vao::~Vao()
{
    glDeleteVertexArrays(1, &_id);
}

void tchip::Vao::create()
{
    glGenVertexArrays(1, &_id);
}

void tchip::Vao::bind()
{
    glBindVertexArray(_id);
}

void tchip::Vao::release()
{
    glBindVertexArray(0);
}
