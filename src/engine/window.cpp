#include "window.h"

#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>
#include <cmath>

#ifdef __EMSCRIPTEN__
#   include <emscripten.h>
#endif

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>

using namespace std;

void glfw_error_callback(int error, const char* description)
{
    cerr << "[GLFW error] " << description << '\n';
}


tchip::Window::Window(const std::string& title, int width_, int height_)
{
    width = width_;
    height = height_;

    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
    {
        throw runtime_error("glfwInit");
    }

    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    //glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, false);
    window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
    if (!window)
    {
        throw runtime_error("glfwCreateWindow");
    }

    glfwMakeContextCurrent(window);
#ifndef __EMSCRIPTEN__
    glfwSwapInterval(0); // vsync: 0 => off, 1 => on
#endif
    glfwSetWindowUserPointer(window, reinterpret_cast<void *>(this));

    // Init glad loader
#ifdef __EMSCRIPTEN__
    if (!gladLoadGLES2Loader((GLADloadproc)glfwGetProcAddress))
#else
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
#endif
    {
        throw runtime_error("gladLoadGLLoader");
    }

#ifndef __EMSCRIPTEN__
    glDebugMessageCallbackARB(oglErrorProxy, this);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
#endif

    // FPS
    // TODO std::memset
    for (int i = 0; i < fpsSize; ++i)
    {
        fpsList[i] = 0;
    }

    // Set callbacks
    glfwSetFramebufferSizeCallback(window, framebufferSizeProxy);
    glfwSetKeyCallback(window, keyProxy);
    glfwSetScrollCallback(window, mouseWheelProxy);

    glfwGetFramebufferSize(window, &width, &height);
    resize(width, height);
}

tchip::Window::~Window()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void APIENTRY tchip::Window::oglErrorProxy(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message, const void* userParam)
{
    const Window* handler = reinterpret_cast<const Window*>(userParam);
    if (handler)
        handler->oglError(source, type, id, severity, length, message);
}

void tchip::Window::oglError(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char* message) const
{
    cerr << "[GL ";
    switch (severity)
    {
        case GL_DEBUG_SEVERITY_LOW:
            cerr << "low";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            cerr << "medium";
            break;
        case GL_DEBUG_SEVERITY_HIGH:
            cerr << "high";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            cerr << "notification";
            break;
        default:
        case GL_DONT_CARE:
            cerr << "dont care";
            break;
    }
    cerr << "] " << message << '\n';
}

void tchip::Window::framebufferSizeProxy(GLFWwindow *window, int w, int h)
{
    Window* handler = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    if (handler)
        handler->resize(w, h);
}

void tchip::Window::resize(int w, int h)
{
    width = w;
    height = h;
    glViewport(0, 0, w, h);
}

void tchip::Window::mouseWheelProxy(GLFWwindow *window, double x, double y)
{
    Window* handler = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    if (handler)
        handler->mouseWheel(x, y);
}

void tchip::Window::keyProxy(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    Window* handler = reinterpret_cast<Window*>(glfwGetWindowUserPointer(window));
    if (handler)
        handler->key(key, scancode, action, mods);
}

void tchip::Window::printOglInfo() const
{
    cout << "OpenGL: " << endl
        << "  - version: " << glGetString(GL_VERSION) << endl
        << "  - vendor: " << glGetString(GL_VENDOR) << endl
        << "  - renderer: " << glGetString(GL_RENDERER) << endl
        << "  - shading version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}

void tchip::Window::cursorShow()
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void tchip::Window::cursorHide()
{
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

glm::vec2 tchip::Window::getMousePosition()
{
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    return glm::vec2(x, y);
}

void tchip::Window::setMousePosition(glm::vec2 v)
{
    glfwSetCursorPos(window, v.x, v.y);
}

glm::ivec2 tchip::Window::getSize()
{
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    return glm::ivec2(w, h);
}

int tchip::Window::run()
{
    initOgl();

#ifdef __EMSCRIPTEN__

    emscripten_set_main_loop_arg(wasm_update, this, 0, true);

#else

    while (!glfwWindowShouldClose(window))
    {
        if (update())
        {
            glfwSwapBuffers(window);
        }
        glfwPollEvents();
    }

#endif

    return 0;
}

#ifdef __EMSCRIPTEN__

void tchip::Window::wasm_update(void *arg)
{
    Window *w = reinterpret_cast<Window*>(arg);
    if (!glfwWindowShouldClose(w->window))
    {
        if (w->update())
        {
            glfwSwapBuffers(w->window);
        }
        glfwPollEvents();
    }
}

#endif

void tchip::Window::close()
{
#ifdef __EMSCRIPTEN__
    emscripten_cancel_main_loop();
#else
    glfwSetWindowShouldClose(window, true);
#endif
}

void tchip::Window::initOgl()
{
    glClearColor(0.39f, 0.58f, 0.93f, 1);
    initialize();
}

double tchip::Window::computeFps(int value)
{
    fpsSum -= fpsList[fpsIndex];
    fpsSum += value;
    fpsList[fpsIndex] = value;

    if (++fpsIndex == fpsSize)
        fpsIndex = 0;

    return ((double)fpsSum / fpsSize);
}
