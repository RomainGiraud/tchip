#ifndef TCHIP_SHADERPROGRAM_H
#define TCHIP_SHADERPROGRAM_H

#include <GL/glad.h>

#include <engine/shader.h>

#include <string>

#include <glm/glm.hpp>

namespace tchip
{

class ShaderProgram
{
public:
    ShaderProgram();
    ~ShaderProgram();

    void create();
    void attachShader(const Shader &shader);
    void detachShader(const Shader &shader);
    void bind();
    void release();
    void link();

    int uniformLocation(const std::string& name, bool exceptionOnError);
    void setUniformValue(const std::string& name, int value, bool exceptionOnError = true);
    void setUniformValue(int location, int value);
    void setUniformValue(const std::string& name, float value, bool exceptionOnError = true);
    void setUniformValue(int location, float value);
    void setUniformValue(const std::string& name, const glm::vec3& value, bool exceptionOnError = true);
    void setUniformValue(int location, const glm::vec3& value);
    void setUniformValue(const std::string& name, const glm::vec4& value, bool exceptionOnError = true);
    void setUniformValue(int location, const glm::vec4& value);
    void setUniformValue(const std::string& name, const glm::mat3& value, bool exceptionOnError = true);
    void setUniformValue(int location, const glm::mat3& value);
    void setUniformValue(const std::string& name, const glm::mat4& value, bool exceptionOnError = true);
    void setUniformValue(int location, const glm::mat4& value);

    int attributeLocation(const std::string& name, bool exceptionOnError);
    void enableVertexAttribArray(const std::string& name, bool exceptionOnError = true);
    void enableVertexAttribArray(int location);
    void disableVertexAttribArray(const std::string& name, bool exceptionOnError = true);
    void disableVertexAttribArray(int location);
    void vertexAttribPointer(const std::string& name, int size, GLenum type, bool normalized, int stride, int offset, bool exceptionOnError = true);
    void vertexAttribPointer(int location, int size, GLenum type, bool normalized, int stride, int offset);

private:
    GLuint _id;
};

}

#endif /*TCHIP_SHADERPROGRAM_H*/
