#ifndef TCHIP_CAMERA_H
#define TCHIP_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

namespace tchip
{

class Camera
{
public:
    Camera();
    ~Camera();

    void update(double time);

    float getZoom() const;
    void setZoom(float z);

    glm::vec3 getPosition() const;
    void setPosition(glm::vec3 p);

    glm::vec3 getTargetPosition() const;
    void setTargetPosition(glm::vec3 p);

    void setSize(glm::ivec2 size);

    glm::mat4 getProjectionMatrix() const;
    glm::mat4 getViewMatrix() const;

protected:
    glm::vec3 _position;
    float _zoom;
    glm::quat _orientation;

    glm::vec3 _targetPosition;
    glm::quat _targetOrientation;

    glm::mat4 _projectionMatrix;
    glm::mat4 _viewMatrix;

    glm::vec3 _target;
    glm::vec3 _direction;

    glm::ivec2 _size;

    void updateProjectionMatrix();
    void updateViewMatrix();
};

}

#endif /*TCHIP_CAMERA_H*/
