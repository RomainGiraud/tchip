#include "shader_program.h"

#include <iostream>
#include <exception>

#include <glm/gtc/type_ptr.hpp>

using namespace std;

tchip::ShaderProgram::ShaderProgram()
    : _id(0)
{
}

tchip::ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(_id);
}

void tchip::ShaderProgram::create()
{
    _id = glCreateProgram();
}

void tchip::ShaderProgram::attachShader(const Shader &shader)
{
    glAttachShader(_id, shader.id());
}

void tchip::ShaderProgram::detachShader(const Shader &shader)
{
    glDetachShader(_id, shader.id());
}

void tchip::ShaderProgram::bind()
{
    glUseProgram(_id);
}

void tchip::ShaderProgram::release()
{
    glUseProgram(0);
}

void tchip::ShaderProgram::link()
{
    glLinkProgram(_id);

    GLint result;
    glGetProgramiv(_id, GL_LINK_STATUS, &result);

    if (!result)
    {
        char infoLog[1024];
        glGetProgramInfoLog(_id, 1024, NULL, infoLog);
        cout << "Program linking at " << _id << ":" << endl
             << infoLog << endl;

        glDeleteProgram(_id);

        throw runtime_error("createShaders: glLinkProgram");
    }
}

int tchip::ShaderProgram::uniformLocation(const string& name, bool exceptionOnError)
{
    int id = glGetUniformLocation(_id, name.c_str());

    if (id == -1)
    {
        if (!exceptionOnError)
            return id;

        throw runtime_error("Unknown uniform location \"" + name + "\"");
    }

    return id;
}

void tchip::ShaderProgram::setUniformValue(const string& name, int value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, int value)
{
    glUniform1i(location, value);
}

void tchip::ShaderProgram::setUniformValue(const string& name, float value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, float value)
{
    glUniform1f(location, value);
}

void tchip::ShaderProgram::setUniformValue(const string& name, const glm::vec3& value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, const glm::vec3& value)
{
    glUniform3fv(location, 1, glm::value_ptr(value));
}

void tchip::ShaderProgram::setUniformValue(const string& name, const glm::vec4& value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, const glm::vec4& value)
{
    glUniform4fv(location, 1, glm::value_ptr(value));
}

void tchip::ShaderProgram::setUniformValue(const string& name, const glm::mat3& value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, const glm::mat3& value)
{
    glUniformMatrix3fv(location, 1, false, glm::value_ptr(value));
}

void tchip::ShaderProgram::setUniformValue(const string& name, const glm::mat4& value, bool exceptionOnError)
{
    setUniformValue(uniformLocation(name, exceptionOnError), value);
}

void tchip::ShaderProgram::setUniformValue(int location, const glm::mat4& value)
{
    glUniformMatrix4fv(location, 1, false, glm::value_ptr(value));
}

int tchip::ShaderProgram::attributeLocation(const string& name, bool exceptionOnError)
{
    int id = glGetAttribLocation(_id, name.c_str());

    if (id == -1)
    {
        if (!exceptionOnError)
            return id;

        throw runtime_error("Unknown attribute location \"" + name + "\"");
    }

    return id;
}

void tchip::ShaderProgram::enableVertexAttribArray(const string& name, bool exceptionOnError)
{
    enableVertexAttribArray(attributeLocation(name, exceptionOnError));
}

void tchip::ShaderProgram::enableVertexAttribArray(int location)
{
    glEnableVertexAttribArray(location);
}

void tchip::ShaderProgram::disableVertexAttribArray(const string& name, bool exceptionOnError)
{
    disableVertexAttribArray(attributeLocation(name, exceptionOnError));
}

void tchip::ShaderProgram::disableVertexAttribArray(int location)
{
    glDisableVertexAttribArray(location);
}

void tchip::ShaderProgram::vertexAttribPointer(const string& name, int size, GLenum type, bool normalized, int stride, int offset, bool exceptionOnError)
{
    vertexAttribPointer(attributeLocation(name, exceptionOnError), size, type, normalized, stride, offset);
}

void tchip::ShaderProgram::vertexAttribPointer(int location, int size, GLenum type, bool normalized, int stride, int offset)
{
    glVertexAttribPointer(location, size, type, normalized, stride, reinterpret_cast<void*>(offset));
}
