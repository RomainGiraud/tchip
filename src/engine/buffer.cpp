#include "buffer.h"

tchip::Buffer::Buffer(Type type, Usage usage)
    : _id(0)
{
    switch (type)
    {
        case Type::Vertex:
            _type = GL_ARRAY_BUFFER;
            break;
        case Type::Index:
            _type = GL_ELEMENT_ARRAY_BUFFER;
            break;
    }

    switch (usage)
    {
        case Usage::StreamDraw:
            _usage = GL_STREAM_DRAW;
            break;
        case Usage::StaticDraw:
            _usage = GL_STATIC_DRAW;
            break;
        case Usage::DynamicDraw:
            _usage = GL_DYNAMIC_DRAW;
            break;
        case Usage::StreamRead:
            _usage = GL_STREAM_READ;
            break;
        case Usage::StaticRead:
            _usage = GL_STATIC_READ;
            break;
        case Usage::DynamicRead:
            _usage = GL_DYNAMIC_READ;
            break;
        case Usage::StreamCopy:
            _usage = GL_STREAM_COPY;
            break;
        case Usage::StaticCopy:
            _usage = GL_STATIC_COPY;
            break;
        case Usage::DynamicCopy:
            _usage = GL_DYNAMIC_COPY;
            break;
    }
}

tchip::Buffer::~Buffer()
{
    glDeleteBuffers(1, &_id);
}

void tchip::Buffer::create()
{
    glGenBuffers(1, &_id);
}

void tchip::Buffer::bind()
{
    glBindBuffer(_type, _id);
}

void tchip::Buffer::release()
{
    glBindBuffer(_type, 0);
}

void tchip::Buffer::attach(const void *data, size_t size)
{
    bind();
    glBufferData(_type, size, data, _usage);
}
