#include <iostream>

#include <unistd.h>

#include <engine/window.h>
#include <engine/buffer.h>
#include <engine/shader_program.h>
#include <engine/vao.h>
#include <interpreter/interpreter.h>

using namespace std;
using namespace tchip;

float map(float value,
          float start1, float stop1,
          float start2, float stop2)
{
    return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
}

class TChip : public Window
{
public:
    explicit TChip(const std::string &game)
        : Window("tchip", width * 10, height * 10)
        , vertices(Buffer::Type::Vertex)
        , indices(Buffer::Type::Index)
        , vshader(Shader::Type::Vertex)
        , fshader(Shader::Type::Fragment)
    {
        ipt.loadFile(game);
    }

    void key(int key, int scancode, int action, int mods)
    {
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
            close();
    }

    void initialize()
    {
        vao.create();
        vao.bind();


        float v[] = {
            0.0f, 0.0f,
            0.9999f, 0.0f,
            0.0f, 0.9999f,
            0.9999f, 0.9999f,

        };
        vertices.create();
        vertices.attach(v, sizeof(v));

        unsigned int ind[] = {
            0, 1, 2, 3,
        };
        indices.create();
        indices.attach(ind, sizeof(ind));


        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, &ipt.getScreenData()[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        vshader.create();
        vshader.loadSource(vertex_shader);
        fshader.create();
        fshader.loadSource(fragment_shader);
        program.create();
        program.attachShader(vshader);
        program.attachShader(fshader);
        program.link();
        program.bind();

        program.enableVertexAttribArray(0);
        program.vertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        program.setUniformValue("data", 0);
    }

    bool update()
    {
        if (!ipt.run())
        {
            close();
            return false;
        }

        for (size_t i = 0; i < keys.size(); ++i)
        {
            int state = glfwGetKey(window, keys[i]);
            if (state == GLFW_PRESS)
            {
                ipt.keyPressed(i);
                if (ipt.waitingForKeyPress())
                {
                    // cout << "Waiting for key press\n";
                    ipt.injectKeyPress(i);
                }
            }
        }

        // if (!ipt.refresh())
        // {
        //     return false;
        // }


        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, &ipt.getScreenData()[0]);

        program.bind();
        vao.bind();
        indices.bind();
        glDrawElementsInstanced(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT, 0, width * height);

        return true;
    }

private:
    Interpreter ipt;

    static constexpr std::array<int, 16> keys = {
        GLFW_KEY_1,
        GLFW_KEY_2,
        GLFW_KEY_3,
        GLFW_KEY_4,
        GLFW_KEY_Q,
        GLFW_KEY_W,
        GLFW_KEY_E,
        GLFW_KEY_R,
        GLFW_KEY_A,
        GLFW_KEY_S,
        GLFW_KEY_D,
        GLFW_KEY_F,
        GLFW_KEY_Z,
        GLFW_KEY_X,
        GLFW_KEY_C,
        GLFW_KEY_V,
    };

    static constexpr size_t width = 64, height = 32;
    Buffer vertices, indices;
    Shader vshader, fshader;
    ShaderProgram program;
    Vao vao;

    unsigned int texture;

    const char* vertex_shader = R"(#version 300 es
precision mediump float;
precision mediump usampler2D;

in vec2 aPos;
out vec4 color;

uniform usampler2D data;

float map(float value,
          float start1, float stop1,
          float start2, float stop2)
{
    return start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
}

void main()
{
    ivec2 tsz = textureSize(data, 0);
    float x = aPos.x + float(int(gl_InstanceID % 64));
    float y = aPos.y + float(int(gl_InstanceID / 64));

    gl_Position.x = map(x, 0.0, float(tsz.x), -1.0, 1.0);
    gl_Position.y = map(y, 0.0, float(tsz.y), -1.0, 1.0);
    gl_Position.z = 0.0;
    gl_Position.w = 1.0;

    ivec2 pos = ivec2(int(x), tsz.y - int(y));
    uint c = texelFetch(data, pos, 0).r;
    if (c < uint(1))
        color = vec4(0, 1, 0, 1);
    else
        color = vec4(0, 0, 1, 1);
}
    )";
    const char* fragment_shader = R"(#version 300 es
precision mediump float;

in vec4 color;
out vec4 fragColor;

void main()
{
    fragColor = color;
}
    )";
};

int main(int argc, const char* argv[])
{
    try
    {
#ifdef __EMSCRIPTEN__
        TChip w("WIPEOFF");
#else
        if (argc != 2)
        {
            cout << "Usage: " << argv[0] << " <rom>" << '\n';
            return 2;
        }
        TChip w(argv[1]);
#endif

        w.printOglInfo();
        w.run();
    }
    catch (const exception& e)
    {
        cerr << "[error] " << e.what() << '\n';
        return 1;
    }

    return 0;
}
