#ifndef TCHIP_INTERPRETER_H
#define TCHIP_INTERPRETER_H

#include <array>
#include <string>
#include <stack>
#include <memory>
#include <random>
#include <optional>
#include <chrono>

namespace tchip
{

class Interpreter
{
public:
    Interpreter();

    void loadFile(const std::string& filename);
    void loadSource(const uint8_t* source, size_t n);
    bool run();

    void setRandomSeed(int seed);
    bool setPixels(const uint8_t* start, int count, int x, int y);

    const std::array<uint8_t, 64 * 32>& getScreenData() const { return screen; }
    uint8_t getRegister(int n) const { return registers[n]; }
    uint16_t getRegisterI() const { return register_i; }
    uint8_t getMemoryAt(uint16_t pointer) const { return program[pointer]; }
    uint8_t getDelayTimer() const { return timer; }
    uint8_t getSoundTimer() const { return sound_timer; }

    bool waitingForKeyPress() const;
    void injectKeyPress(uint8_t key);
    void keyPressed(uint8_t key);
    bool refresh() { return need_refresh; }

private:
    static const uint16_t start = 0x200;
    uint16_t pointer = start;
    std::array<uint8_t, 16> registers;
    std::array<bool, 16> key_pressed;
    uint16_t register_i;
    uint8_t timer;
    uint8_t sound_timer;

    std::array<uint8_t, 64 * 32> screen;
    bool need_refresh = false;

    size_t program_size = 0;
    static const size_t memory_size = 0xFFF;
    std::array<uint8_t, memory_size> program;
    std::stack<uint16_t> call_stack;

    std::optional<uint8_t> waiting_for_key_press;

    std::mt19937 random_engine;
    std::uniform_int_distribution<uint8_t> uniform_dist;

    static constexpr double frequency = 500.0f;
    static constexpr std::chrono::microseconds frame_time_us = std::chrono::microseconds { (int)((1 / frequency) * 1000 * 1000) };
    std::chrono::time_point<std::chrono::high_resolution_clock> previous_frame_time = std::chrono::high_resolution_clock::now();
    static constexpr double timer_frequency = 60.0f;
    static constexpr std::chrono::microseconds timers_us = std::chrono::microseconds { (int)((1 / timer_frequency) * 1000 * 1000) };
    std::chrono::time_point<std::chrono::high_resolution_clock> previous_timers = std::chrono::high_resolution_clock::now();
    double timers_offset = 0;

    void initialize();
};

}

#endif /*TCHIP_INTERPRETER_H*/
