#include "interpreter.h"

#include <tools/helpers.h>
#include <tools/scope_guard.h>

#include <random>
#include <stack>
#include <thread>
#include <stdexcept>
#include <cstring>
#include <cstdarg>

#ifndef __EMSCRIPTEN__
#include <doctest/doctest.h>
#endif

using namespace std;

#ifndef NDEBUG
__attribute__((format(printf, 1, 2)))
void log(const char* format, ...)
{
    va_list arglist;
    va_start(arglist, format);
    vfprintf(stderr, format, arglist);
    va_end(arglist);
}
#else
void log(const char*, ...) {}
#endif


tchip::Interpreter::Interpreter()
    : uniform_dist(0x0, 0xff)
{
    random_device rd;
    setRandomSeed(rd());
}

void tchip::Interpreter::setRandomSeed(int seed)
{
    random_engine.seed(seed);
    uniform_dist.reset();
}

bool tchip::Interpreter::waitingForKeyPress() const
{
    return waiting_for_key_press.has_value();
}

void tchip::Interpreter::injectKeyPress(uint8_t key)
{
    if (!waitingForKeyPress())
        return;

    registers[*waiting_for_key_press] = key;
    waiting_for_key_press.reset();
}

void tchip::Interpreter::keyPressed(uint8_t key)
{
    key_pressed[key] = true;
}

void tchip::Interpreter::initialize()
{
    pointer = start;
    registers.fill(0);
    register_i = 0;
    timer = 0;
    sound_timer = 0;
    screen.fill(0);
    program.fill(0);
    waiting_for_key_press.reset();
    key_pressed.fill(false);
    previous_timers = std::chrono::high_resolution_clock::now();

    unsigned char chip8_fontset[80] =
    {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
    };
    memcpy((char*)&program[0], &chip8_fontset[0], 80);
}

void tchip::Interpreter::loadFile(const std::string& filename)
{
    initialize();

    program_size = tools::helpers::getFileContentInto(filename, (char*)&program[0], memory_size, start);
    if (program_size == 0)
        throw runtime_error("game is too big for chip8 memory");
}

void tchip::Interpreter::loadSource(const uint8_t* source, size_t n)
{
    initialize();

    program_size = n;
    memcpy((char*)&program[start], source, program_size);
}

bool tchip::Interpreter::run()
{
    tools::ScopeGuard sg([this]{
        key_pressed.fill(false);
    });

    // end of the program
    if (pointer >= memory_size)
    {
        return false;
    }

    // For the moment, we do not need to refresh the screen.
    need_refresh = false;

    // Run interpreter at (almost) the right frequency.
    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::micro> elapsed = now - previous_frame_time;
    if (elapsed < frame_time_us)
    {
        std::this_thread::sleep_for(frame_time_us - elapsed);
    }
    previous_frame_time = std::chrono::high_resolution_clock::now();

    // The program wait for a key press.
    if (waiting_for_key_press.has_value())
    {
        log("waiting for a key press to store in V%d\n", *waiting_for_key_press);
        return true;
    }

    // Decrease timers (delay and sound) at the right frequency.
    auto timer_now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::micro> timer_elapsed = timer_now - previous_timers;
    double timers_tick = timer_elapsed.count() / timers_us.count();
    if ((timers_tick + timers_offset) >= 1)
    {
        timers_offset = (timers_tick + timers_offset) - 1;

        if (timer > 0)
            timer -= 1;

        if (sound_timer > 0)
            sound_timer -= 1;

        previous_timers = timer_now;
    }


    const uint8_t c1 = program[pointer];
    const uint8_t c2 = program[pointer + 1];
    pointer += 2;

    log("-- cmd: 0x%02x%02x\n", c1, c2);

    const uint8_t b1 = (c1 & 0xf0) >> 4;
    const uint8_t b2 = c1 & 0x0f;
    const uint8_t b3 = (c2 & 0xf0) >> 4;
    const uint8_t b4 = c2 & 0x0f;

    switch (b1)
    {
        case 0x6:
            {
                log("store %d in V%d\n", c2, b2);
                registers[b2] = c2;
            }
            break;
        case 0x8:
            switch (b4)
            {
                case 0x0:
                    {
                        log("store V%d in V%d\n", b3, b2);
                        registers[b2] = registers[b3];
                    }
                    break;
                case 0x4:
                    {
                        log("add V%d to V%d\n", b3, b2);
                        int v = registers[b2] + registers[b3];
                        registers[0xf] = (v > 0xff) ? 1 : 0;
                        registers[b2] = v;
                    }
                    break;
                case 0x5:
                    {
                        log("substract V%d to V%d\n", b3, b2);
                        registers[0xf] = (registers[b2] < registers[b3]) ? 0 : 1;
                        registers[b2] -= registers[b3];
                    }
                    break;
                case 0x7:
                    {
                        log("set V%d to (V%d - V%d)\n", b2, b3, b2);
                        registers[0xf] = (registers[b3] < registers[b2]) ? 0 : 1;
                        registers[b2] = registers[b3] - registers[b2];
                    }
                    break;
                case 0x2:
                    {
                        log("set V%d to (V%d & V%d)\n", b2, b2, b3);
                        registers[b2] &= registers[b3];
                    }
                    break;
                case 0x1:
                    {
                        log("set V%d to (V%d | V%d)\n", b2, b2, b3);
                        registers[b2] |= registers[b3];
                    }
                    break;
                case 0x3:
                    {
                        log("set V%d to (V%d ^ V%d)\n", b2, b2, b3);
                        registers[b2] ^= registers[b3];
                    }
                    break;
                case 0x6:
                    {
                        log("set V%d to V%d right shifted\n", b2, b3);
                        registers[0xf] = (registers[b3] & 0x1);
                        registers[b2] = registers[b3] >> 1;
                    }
                    break;
                case 0xe:
                    {
                        log("set V%d to V%d left shifted\n", b2, b3);
                        registers[0xf] = (registers[b3] & 0x80) >> 7;
                        registers[b2] = registers[b3] << 1;
                    }
                    break;
            }
            break;
        case 0x7:
            {
                log("add value %d to V%d\n", c2, b2);
                registers[b2] += c2;
            }
            break;
        case 0xc:
            {
                uint8_t mean = uniform_dist(random_engine);
                registers[b2] = mean & c2;
                log("set V%d to a random number (%d)\n", b2, registers[b2]);
            }
            break;
        case 0x1:
            {
                int v = (b2 << 8) + c2;
                log("jump to %d\n", v);
                pointer = v;
            }
            break;
        case 0xb:
            {
                int v = (b2 << 8) + c2 + registers[0x0];
                log("jump to (%d + V0)\n", v);
                pointer = v;
            }
            break;
        case 0x2:
            {
                int v = (b2 << 8) + c2;
                log("execute subroutine at V%d\n", v);
                call_stack.push(pointer);
                pointer = v;
            }
            break;
        case 0x0:
            {
                if (((b2 << 8) + c2) == 0x0ee)
                {
                    log("return from subroutine\n");
                    pointer = call_stack.top();
                    call_stack.pop();
                }
                else if (((b2 << 8) + c2) == 0x0e0)
                {
                    log("clear the screen\n");
                    screen.fill(0);
                }
                else
                {
                    log("--DEPRECATED-- execute machine language subroutine\n");
                }
            }
            break;
        case 0x3:
            {
                log("skip if V%d(%d) == %d\n", b2, registers[b2], c2);
                if (registers[b2] == c2)
                    pointer += 2;
            }
            break;
        case 0x5:
            {
                switch (b4)
                {
                    case 0x0:
                        {
                            log("skip if V%d == V%d\n", b2, b3);
                            if (registers[b2] == registers[b3])
                                pointer += 2;
                        }
                        break;
                }
            }
            break;
        case 0x4:
            {
                log("skip if V%d != %d\n", b2, c2);
                if (registers[b2] != c2)
                    pointer += 2;
            }
            break;
        case 0x9:
            {
                switch (b4)
                {
                    case 0x0:
                        {
                            log("skip if V%d != V%d\n", b2, b3);
                            if (registers[b2] != registers[b3])
                                pointer += 2;
                        }
                        break;
                }
            }
            break;
        case 0xf:
            {
                switch (c2)
                {
                    case 0x15:
                        {
                            log("set delay timer to V%d\n", b2);
                            timer = registers[b2];
                        }
                        break;
                    case 0x07:
                        {
                            log("store delay timer to V%d\n", b2);
                            registers[b2] = timer;
                        }
                        break;
                    case 0x18:
                        {
                            log("set sound timer to V%d\n", b2);
                            sound_timer = registers[b2];
                        }
                        break;
                    case 0x0a:
                        {
                            log("wait for a key press to store in V%d\n", b2);
                            waiting_for_key_press = b2;
                        }
                        break;
                    case 0x1e:
                        {
                            log("add value from V%d to register I\n", b2);
                            register_i += registers[b2];
                        }
                        break;
                    case 0x29:
                        {
                            log("set memory address to hex digi in V%d (%d)\n", b2, registers[b2]);
                            register_i = registers[b2] * 5;
                        }
                        break;
                    case 0x33:
                        {
                            log("convert hex in V%d (%d) to BCD at %d\n", b2, registers[b2], register_i);

                            int v = registers[b2];
                            uint8_t n3 = v % 10;
                            v /= 10;
                            uint8_t n2 = v % 10;
                            v /= 10;
                            uint8_t n1 = v % 10;

                            program[register_i + 0] = n1;
                            program[register_i + 1] = n2;
                            program[register_i + 2] = n3;
                        }
                        break;
                    case 0x55:
                        {
                            log("Store the values of registers V0 to V%d inclusive at %d\n", b2, register_i);

                            for (uint8_t i = 0; i <= b2; ++i)
                            {
                                program[register_i++] = registers[i];
                            }
                        }
                        break;
                    case 0x65:
                        {
                            log("Fill registers V0 to V%d inclusive with memory at %d\n", b2, register_i);

                            for (uint8_t i = 0; i <= b2; ++i)
                            {
                                registers[i] = program[register_i++];
                            }
                        }
                        break;
                }
            }
            break;
        case 0xe:
            {
                switch (c2)
                {
                    case 0x9e:
                        {
                            log("skip if key in V%d(%d) is pressed\n", b2, registers[b2]);
                            if (key_pressed[registers[b2]])
                                pointer += 2;
                        }
                        break;
                    case 0xa1:
                        {
                            log("skip if key in V%d is not pressed\n", b2);
                            if (!key_pressed[registers[b2]])
                                pointer += 2;
                        }
                        break;
                }
            }
            break;
        case 0xa:
            {
                int v = (b2 << 8) + c2;
                log("store %d in register I\n", v);
                register_i = v;
            }
            break;
        case 0xd:
            {
                log("draw %d bytes at (%d, %d) starting at %d\n", b4, registers[b2], registers[b3], program[register_i]);
                registers[0xf] = (setPixels(&program[register_i], b4, registers[b2], registers[b3]) ? 1 : 0);
            }
            break;
        default:
            {
                log("unknown command: %d\n", b1);
            }
    }

    return true;
}

bool tchip::Interpreter::setPixels(const uint8_t* start, int count, int x, int y)
{
    // printf("Set pixels: %d %d\n", x, y);
    // for (int yy = 0; yy < count; ++yy)
    // {
    //     for (int xx = 0; xx < 8; ++xx)
    //     {
    //         printf("%d", screen[(y + yy) * 64 + (x + xx)]);
    //     }
    //     printf("\n");
    // }

    need_refresh = true;

    bool toggle = false;
    for (int yy = 0; yy < count; ++yy)
    {
        uint8_t s = start[yy];
        for (unsigned n = 0; n < sizeof(uint8_t) * 8; ++n)
        {
            uint8_t mask = 1 << (sizeof(uint8_t) * 8 - n - 1);
            uint8_t new_value = ((s & mask) >> (8 - n - 1));
            int index = (y + yy) * 64 + x + n;

            if (new_value == 1)
            {
                if (screen[index] == 1)
                {
                    toggle = true;
                    screen[index] = 0;
                }
                else
                {
                    screen[index] = 1;
                }
            }
            else
            {
                if (screen[index] == 0)
                {
                    screen[index] = 0;
                }
                else
                {
                    screen[index] = 1;
                }
            }
        }
    }

    // printf("Results:\n");
    // for (int yy = 0; yy < count; ++yy)
    // {
    //     for (int xx = 0; xx < 8; ++xx)
    //     {
    //         printf("%d", screen[(y + yy) * 64 + (x + xx)]);
    //     }
    //     printf("\n");
    // }

    return toggle;
}


#ifndef __EMSCRIPTEN__

TEST_CASE("[6XNN] Store number NN in register VX") {
    const uint8_t source[] = {
        0x60, 0xaf,
        0x6f, 0x14,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x0) == 0xaf);

    CHECK(ipt.run());
    CHECK(ipt.getRegister(0xf) == 0x14);
}

TEST_CASE("[8XY0] Store the value of register VY in register VX") {
    const uint8_t source[] = {
        0x61, 0xe2,
        0x8a, 0x10,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0xa) == 0xe2);
}

TEST_CASE("[7XNN] Add the value NN to register VX") {
    const uint8_t source[] = {
        0x64, 0x23,
        0x74, 0x10,
        0x74, 0xee,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x33);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x21);
}

TEST_CASE("[8XY4] Add the value of register VY to register VX") {
    // Set VF to 01 if a carry occurs
    // Set VF to 00 if a carry does not occur

    const uint8_t source[] = {
        0x64, 0x23,
        0x65, 0x23,
        0x66, 0xdf,
        0x84, 0x54,
        0x84, 0x64,
        0x84, 0x54,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x46);
    CHECK(ipt.getRegister(0xf) == 0x00);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x25);
    CHECK(ipt.getRegister(0xf) == 0x01);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x48);
    CHECK(ipt.getRegister(0xf) == 0x00);
}

TEST_CASE("[8XY5] Subtract the value of register VY from register VX") {
    // Set VF to 00 if a borrow occurs
    // Set VF to 01 if a borrow does not occur

    const uint8_t source[] = {
        0x64, 0x23,
        0x65, 0x10,
        0x66, 0x2f,
        0x84, 0x55,
        0x84, 0x65,
        0x86, 0x55,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x13);
    CHECK(ipt.getRegister(0xf) == 0x01);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0xe4);
    CHECK(ipt.getRegister(0xf) == 0x00);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x6) == 0x1f);
    CHECK(ipt.getRegister(0xf) == 0x01);
}

TEST_CASE("[8XY7] Set register VX to the value of VY minus VX") {
    // Set VF to 00 if a borrow occurs
    // Set VF to 01 if a borrow does not occur

    const uint8_t source[] = {
        0x64, 0x23,
        0x65, 0x10,
        0x66, 0x2f,
        0x85, 0x47,
        0x86, 0x57,
        0x85, 0x47,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x13);
    CHECK(ipt.getRegister(0xf) == 0x01);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x6) == 0xe4);
    CHECK(ipt.getRegister(0xf) == 0x00);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x10);
    CHECK(ipt.getRegister(0xf) == 0x01);
}

TEST_CASE("[8XY2] Set VX to VX AND VY") {
    const uint8_t source[] = {
        0x64, 0b11001010,
        0x65, 0b01010110,
        0x84, 0x52,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0b01000010);
}

TEST_CASE("[8XY1] Set VX to VX OR VY") {
    const uint8_t source[] = {
        0x64, 0b11001010,
        0x65, 0b01010110,
        0x84, 0x51,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0b11011110);
}

TEST_CASE("[8XY3] Set VX to VX XOR VY") {
    const uint8_t source[] = {
        0x64, 0b11001010,
        0x65, 0b01010110,
        0x84, 0x53,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0b10011100);
}

TEST_CASE("[8XY6] Store the value of register VY shifted right one bit in register VX") {
    // Set register VF to the least significant bit prior to the shift

    const uint8_t source[] = {
        0x64, 0b11001001,
        0x85, 0x46,
        0x85, 0x56,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0b01100100);
    CHECK(ipt.getRegister(0xf) == 0x01);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0b00110010);
    CHECK(ipt.getRegister(0xf) == 0x00);
}

TEST_CASE("[8XYE] Store the value of register VY shifted left one bit in register VX") {
    // Set register VF to the most significant bit prior to the shift

    const uint8_t source[] = {
        0x64, 0b10011011,
        0x85, 0x4e,
        0x85, 0x5e,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0b00110110);
    CHECK(ipt.getRegister(0xf) == 0x01);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0b01101100);
    CHECK(ipt.getRegister(0xf) == 0x00);
}

TEST_CASE("[CXNN] Set VX to a random number with a mask of NN") {
    const uint8_t source[] = {
        0xC4, 0xff,
        0xC4, 0x3a,
    };
    tchip::Interpreter ipt;
    ipt.setRandomSeed(0);
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 140);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x12);
}

TEST_CASE("[1NNN] Jump to address NNN") {
    const uint8_t source[] = {
        0x12, 0x04,
        0x64, 0xed,
        0x65, 0x34,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x0);
    CHECK(ipt.getRegister(0x5) == 0x34);
}

TEST_CASE("[BNNN] Jump to address NNN + V0") {
    const uint8_t source[] = {
        0x60, 0x06,
        0xB2, 0x00,
        0x64, 0xed,
        0x65, 0x34,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x0);
    CHECK(ipt.getRegister(0x5) == 0x34);
}

TEST_CASE("[2NNN] Execute subroutine starting at address NNN") {
    const uint8_t source[] = {
        0x22, 0x04,
        0x64, 0xed,
        0x65, 0x34,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x0);
    CHECK(ipt.getRegister(0x5) == 0x34);
}

TEST_CASE("[00EE] Return from a subroutine") {
    const uint8_t source[] = {
        0x22, 0x04,
        0x64, 0xed,
        0x65, 0x34,
        0x00, 0xee,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0x0);
    CHECK(ipt.getRegister(0x5) == 0x34);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x4) == 0xed);
    CHECK(ipt.getRegister(0x5) == 0x34);
}

TEST_CASE("[0NNN] Execute machine language subroutine at address NNN") {
    // Not implemented on this interpreter
}

TEST_CASE("[3XNN] Skip the following instruction if the value of register VX equals NN") {
    const uint8_t source[] = {
        0x64, 0xed,
        0x34, 0xed,
        0x65, 0x12,
        0x66, 0x34,
        0x34, 0xef,
        0x67, 0x12,
        0x68, 0x34,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x0);
    CHECK(ipt.getRegister(0x6) == 0x34);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x12);
    CHECK(ipt.getRegister(0x8) == 0x34);
}

TEST_CASE("[5XY0] Skip the following instruction if the value of register VX is equal to the value of register VY") {
    const uint8_t source[] = {
        0x64, 0xed,
        0x65, 0xed,
        0x66, 0xef,
        0x54, 0x50,
        0x67, 0x34,
        0x68, 0x12,
        0x54, 0x60,
        0x67, 0x34,
        0x68, 0x56,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x0);
    CHECK(ipt.getRegister(0x8) == 0x12);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x34);
    CHECK(ipt.getRegister(0x8) == 0x56);
}

TEST_CASE("[4XNN] Skip the following instruction if the value of register VX is not equal to NN") {
    const uint8_t source[] = {
        0x64, 0xed,
        0x44, 0xef,
        0x65, 0x12,
        0x66, 0x34,
        0x44, 0xed,
        0x67, 0x12,
        0x68, 0x34,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x0);
    CHECK(ipt.getRegister(0x6) == 0x34);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x12);
    CHECK(ipt.getRegister(0x8) == 0x34);
}

TEST_CASE("[9XY0] Skip the following instruction if the value of register VX is not equal to the value of register VY") {
    const uint8_t source[] = {
        0x64, 0xed,
        0x65, 0xef,
        0x66, 0xed,
        0x94, 0x50,
        0x67, 0x34,
        0x68, 0x12,
        0x94, 0x60,
        0x67, 0x34,
        0x68, 0x56,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x0);
    CHECK(ipt.getRegister(0x8) == 0x12);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x34);
    CHECK(ipt.getRegister(0x8) == 0x56);
}

TEST_CASE("[FX15] Set the delay timer to the value of register VX") {
    const uint8_t source[] = {
        0x64, 0x05,
        0xf4, 0x15,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getDelayTimer() == 0x5);
    std::chrono::duration<double> wait ((1.0f/60) * 2);
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getDelayTimer() == 0x3);
}

TEST_CASE("[FX07] Store the current value of the delay timer in register VX") {
    const uint8_t source[] = {
        0x64, 0x05,
        0xf4, 0x15,
        0x00, 0x00,
        0x00, 0x00,
        0xf5, 0x07,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    std::chrono::duration<double> wait ((1.0f/60) * 1);
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x2);
}

TEST_CASE("[FX18] Set the sound timer to the value of register VX") {
    const uint8_t source[] = {
        0x64, 0x07,
        0xf4, 0x18,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getSoundTimer() == 0x7);
    std::chrono::duration<double> wait ((1.0f/60) * 1);
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    std::this_thread::sleep_for(wait);
    CHECK(ipt.run());
    CHECK(ipt.getSoundTimer() == 0x3);
}

TEST_CASE("[FX0A] Wait for a keypress and store the result in register VX") {
    const uint8_t source[] = {
        0xf4, 0x0a,
        0x00, 0x00,
        0xf5, 0x0a,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.waitingForKeyPress());
    ipt.injectKeyPress(0x3);
    CHECK(ipt.getRegister(0x4) == 0x3);
    CHECK(ipt.run());
    CHECK(!ipt.waitingForKeyPress());
    CHECK(ipt.run());
    CHECK(ipt.waitingForKeyPress());
    ipt.injectKeyPress(0xf);
    CHECK(ipt.getRegister(0x5) == 0xf);
}

TEST_CASE("[EX9E] Skip the following instruction if the key corresponding to the hex value currently stored in register VX is pressed") {
    const uint8_t source[] = {
        0x64, 0x03,
        0xe4, 0x9e,
        0x65, 0xcd,
        0x66, 0xef,
        0xe4, 0x9e,
        0x67, 0xab,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(!ipt.waitingForKeyPress());
    ipt.keyPressed(0x5);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0xcd);
    CHECK(ipt.getRegister(0x6) == 0xef);
    ipt.keyPressed(0x3);
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0x0);
}

TEST_CASE("[EXA1] Skip the following instruction if the key corresponding to the hex value currently stored in register VX is not pressed") {
    const uint8_t source[] = {
        0x64, 0x03,
        0xe4, 0xa1,
        0x65, 0xcd,
        0x66, 0xef,
        0xe4, 0xa1,
        0x67, 0xab,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(!ipt.waitingForKeyPress());
    ipt.keyPressed(0x5);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x5) == 0x0);
    CHECK(ipt.getRegister(0x6) == 0xef);
    ipt.keyPressed(0x3);
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegister(0x7) == 0xab);
}

TEST_CASE("[ANNN] Store memory address NNN in register I") {
    const uint8_t source[] = {
        0xa9, 0xac,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.getRegisterI() == 0x9ac);
}

TEST_CASE("[FX1E] Add the value stored in register VX to register I") {
    const uint8_t source[] = {
        0xa9, 0xac,
        0x64, 0xef,
        0xf4, 0x1e,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegisterI() == 0xa9b);
}

TEST_CASE("[DXYN] Draw a sprite at position VX, VY with N bytes of sprite data starting at the address stored in I") {
    // Set VF to 01 if any set pixels are changed to unset, and 00 otherwise

    const uint8_t source[] = {
        0x60, 0x02,
        0x61, 0x01,
        0xa2, 0x0c,
        0xd0, 0x14,
        0xd0, 0x14,
        0xd0, 0x14,
        0x90, 0x09,
        0x94, 0x49,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));

    //CHECK(memcmp(&ipt.getScreenData()[0], (const uint8_t[]){ 0x90, 0x09 }, 2) == 0);
    auto screencmp = [&ipt, &source](){
        CHECK(ipt.getRegister(0xf) == 0x0);
        const int nb_bytes = 4;
        const int start_source = 12;
        //const int width = 64;
        const int start_x = 2;
        const int start_y = 1;
        for (int y = 0; y < nb_bytes; ++y)
        {
            uint8_t s = source[start_source + y];

            for (unsigned n = 0; n < sizeof(uint8_t) * 8; ++n)
            {
                uint8_t mask = 1 << (sizeof(uint8_t) * 8 - n - 1);
                uint8_t value = ((s & mask) >> (8 - n - 1));
                int index = (start_y + y) * 64 + start_x + n;
                CHECK(value == ipt.getScreenData()[index]);
            }
        }
    };

    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    screencmp();

    CHECK(ipt.run());
    CHECK(ipt.getRegister(0xf) == 0x1);
    for (int y = 0; y < 32; ++y)
    {
        for (int x = 0; x < 64; ++x)
        {
            CHECK(0 == ipt.getScreenData()[y * 64 + x]);
        }
    }

    CHECK(ipt.run());
    screencmp();
}

TEST_CASE("[00E0] Clear the screen") {
    const uint8_t source[] = {
        0xa2, 0x04,
        0xd2, 0x14,
        0x00, 0xe0,
        0x90, 0x09,
        0x94, 0x49,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());

    for (int y = 0; y < 32; ++y)
    {
        for (int x = 0; x < 64; ++x)
        {
            CHECK(0 == ipt.getScreenData()[y * 64 + x]);
        }
    }
}

TEST_CASE("[FX29] Set I to the memory address of the sprite data corresponding to the hexadecimal digit stored in register VX") {
    const uint8_t source[] = {
        0x64, 0x0a,
        0xf4, 0x29,
        0xd0, 0x05,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getRegisterI() == (0x0a * 5));

    uint8_t data_0x0a[] = {
        1, 1, 1, 1,
        1, 0, 0, 1,
        1, 1, 1, 1,
        1, 0, 0, 1,
        1, 0, 0, 1,
    };

    //const int width = 64;
    const int start_x = 0;
    const int start_y = 0;
    for (int y = 0; y < 5; ++y)
    {
        for (int x = 0; x < 4; ++x)
        {
            CHECK(data_0x0a[y * 4 + x] == ipt.getScreenData()[(start_y + y) * 64 + start_x + x]);
        }
    }
}

TEST_CASE("[FX33] Store the binary-coded decimal equivalent of the value stored in register VX at addresses I, I+1, and I+2") {
    const uint8_t source[] = {
        0x64, 0xaf,
        0xf4, 0x33,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() + 0) == 1);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() + 1) == 7);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() + 2) == 5);
}

TEST_CASE("[FX55] Store the values of registers V0 to VX inclusive in memory starting at address I") {
    // I is set to I + X + 1 after operation

    const uint8_t source[] = {
        0xa3, 0x00,
        0x60, 0x12,
        0x61, 0xba,
        0x64, 0xf3,
        0xF4, 0x55,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());

    CHECK(ipt.getRegisterI() == 0x300 + 4 + 1);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 5 + 0) == 0x12);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 5 + 1) == 0xba);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 5 + 2) == 0x0);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 5 + 3) == 0x0);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 5 + 4) == 0xf3);
}

TEST_CASE("[FX65] Fill registers V0 to VX inclusive with the values stored in memory starting at address I") {
    // I is set to I + X + 1 after operation

    const uint8_t source[] = {
        0xa3, 0x00,
        0x64, 0xaf,
        0xf4, 0x33,
        0xf3, 0x65,
    };
    tchip::Interpreter ipt;
    ipt.loadSource(source, sizeof(source) / sizeof(source[0]));
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());
    CHECK(ipt.run());

    CHECK(ipt.getRegisterI() == 0x300 + 3 + 1);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 4 + 0) == 1);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 4 + 1) == 7);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 4 + 2) == 5);
    CHECK(ipt.getMemoryAt(ipt.getRegisterI() - 4 + 3) == 0x0);
}

/*

MAZE

0xa21e // reg_i = 21e
0xc201 // V2 = 1 ou 0
0x3201 // if V2 == 1 skip next
0xa21a // reg_i = 21a
0xd014 // draw (0,1) 4 bytes
0x7004 // add 04 to V0
0x3040 // if V0 == 40 skip next
0x1200 // jump to 200
0x6000 // store 00 in V0
0x7104 // add 04 to V1
0x3120 // if V1 == 20 skip next
0x1200 // jump to 200
0x1218 // jump to 218
0x8040 // store V4 in V0
0x2010 // execute subroutine at 010
0x2040 // execute subroutine at 040
0x8010 // store V1 in V0

*/

#endif /*__EMSCRIPTEN__*/
