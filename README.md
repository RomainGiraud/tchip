# tchip

Another [CHIP-8](https://en.wikipedia.org/wiki/CHIP-8) interpreter, written in modern C++.

> CHIP-8 is an interpreted programming language, developed by Joseph Weisbecker. It was initially used on the COSMAC VIP and Telmac 1800 8-bit microcomputers in the mid-1970s.

It is mainly a fun personal project.

## Installation and running

Download sources and build the project.

You will need the following libraries :
* glm
* glfw3

And to compile tests:
* doctest

I use [CMake](https://cmake.org/) as building system, [vcpkg](https://vcpkg.io) as
dependency tool and [just](https://just.systems/) as command runner.

### Host

```bash
just init
just build
just run # or ./build/tchip ./games/PONG
```

### WebAssembly

You must install [Emscripten](https://emscripten.org/).

```bash
just init
just build-wasm
just run-wam # or python -m http.server
```

## Build in debug modern

```bash
BUILD=Debug just build
```

## Re-generate OpenGL headers

```bash
glad --profile core --out-path src/GL/ --generator c --spec gl --reproducible --local-files --api "gl=3.3,gles2=3.2"
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
